package users

import (
	"book_store_app/bookstore_users-api/domain/users"
	"book_store_app/bookstore_users-api/services"
	"book_store_app/bookstore_users-api/utils/errors"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func TestServiceInterface() {

}

func getUserId(userIDParam string) (int64, *errors.RestErr) {
	userId, userErr := strconv.ParseInt(userIDParam, 10, 64)
	if userErr != nil {
		return 0, errors.NewBadRequestError("user is should be a number")
	}
	return userId, nil
}

func Create(c *gin.Context) {
	var user users.User

	/*bytes, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		//TODO: Error Handler
		return
	}
	if err := json.Unmarshal(bytes, &user); err != nil {
		//TODO: JSON Error Handler
		fmt.Println(err)
		return
	}*/

	if err := c.ShouldBindJSON(&user); err != nil {
		//TODO: JSON Error Handler. Return bad request
		restErr := errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status, restErr)
		return
	}
	result, saveErr := services.UserService.CreateUser(user)
	if saveErr != nil {
		//TODO: Error creation handler
		c.JSON(saveErr.Status, saveErr)
		return
	}
	c.JSON(http.StatusCreated, result.Marshall(c.GetHeader("X-Public") == "true"))
}

func Get(c *gin.Context) {
	userId, idErr := getUserId(c.Param("user_id"))
	if idErr != nil {
		c.JSON(idErr.Status, idErr)
	}

	user, getErr := services.UserService.GetUser(userId)
	if getErr != nil {
		//TODO: Error creation handler
		c.JSON(getErr.Status, getErr)
		return
	}
	c.JSON(http.StatusOK, user.Marshall(c.GetHeader("X-Public") == "true"))
}

func Update(c *gin.Context) {

	userId, idErr := getUserId(c.Param("user_id"))
	if idErr != nil {
		c.JSON(idErr.Status, idErr)
	}

	var user users.User
	if err := c.ShouldBindJSON(&user); err != nil {
		//TODO: JSON Error Handler. Return bad request
		restErr := errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status, restErr)
		return
	}

	user.Id = userId

	isPartial := c.Request.Method == http.MethodPatch

	result, err := services.UserService.UpdateUser(isPartial, user)
	if err != nil {
		c.JSON(err.Status, err)
		return
	}
	c.JSON(http.StatusOK, result.Marshall(c.GetHeader("X-Public") == "true"))

}

func Delete(c *gin.Context) {
	userId, idErr := getUserId(c.Param("user_id"))
	if idErr != nil {
		c.JSON(idErr.Status, idErr)
	}

	if err := services.UserService.DeleteUser(userId); err != nil {
		c.JSON(err.Status, err)
		return
	}
	c.JSON(http.StatusOK, map[string]string{"status": "deleted"})

}

func Search(c *gin.Context) {
	status := c.Query("status")
	users, err := services.UserService.SearchUser(status)
	if err != nil {
		c.JSON(err.Status, err)
		return
	}

	c.JSON(http.StatusOK, users.Marshall(c.GetHeader("X-Public") == "true"))
}

/*func SearchUser(c *gin.Context) {
	c.String(http.StatusNotImplemented, "implement me !")
}*/
