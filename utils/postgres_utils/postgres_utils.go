package postgres_utils

import (
	"book_store_app/bookstore_users-api/utils/errors"
	"fmt"
	"strings"

	"github.com/lib/pq"
)

const (
	errorNoRows = "no rows in result set"
)

func ParseError(err error) *errors.RestErr {

	fmt.Println(fmt.Sprintf(err.Error()))
	if strings.Contains(err.Error(), errorNoRows) {
		return errors.NewNotFoundError("no record matching given id")
	}

	sqlerr, ok := err.(*pq.Error)
	if !ok {
		return errors.NewInternalServerError("error parsing database response")
	}

	fmt.Println(sqlerr.Code)
	switch sqlerr.Code {
	case "23505":
		return errors.NewBadRequestError("invalid data - unique violation")
	case "23502":
		return errors.NewBadRequestError("invalid data - null restriction violation")
	}

	return errors.NewInternalServerError("error procesing request")
}
