package users

import (
	"book_store_app/bookstore_users-api/datasources/postgresql/users_db"
	"book_store_app/bookstore_users-api/logger"
	"book_store_app/bookstore_users-api/utils/errors"
	"fmt"
)

const (
	queryInsertUser       = "INSERT INTO users(first_name, last_name, email, date_created, status, password) VALUES($1, $2, $3, $4, $5, $6) RETURNING id;"
	queryGetUser          = "SELECT id, first_name, last_name, email, date_created, status from users WHERE id = $1;"
	queryUpdateUser       = "UPDATE users SET first_name=$1, last_name=$2, email=$3 WHERE id=$4 RETURNING id;"
	queryDeleteUser       = "DELETE FROM users WHERE id=$1;"
	queryFindUserByStatus = "SELECT id, first_name, last_name, email, date_created, status from users WHERE status = $1;"
)

var (
	userDB = make(map[int64]*User)
)

func (user *User) Get() *errors.RestErr {

	stmt, err := users_db.Client.Prepare(queryGetUser)
	if err != nil {
		logger.Error("error when trying to prepare get user statement", err)
		return errors.NewInternalServerError("database error")
	}
	defer stmt.Close()

	result := stmt.QueryRow(user.Id)
	if err := result.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated, &user.Status); err != nil {
		logger.Error("error when trying to get user by id", err)
		return errors.NewInternalServerError("database error")
	}

	return nil
}

func (user *User) Save() *errors.RestErr {

	stmt, err := users_db.Client.Prepare(queryInsertUser)
	if err != nil {
		logger.Error("error when trying to prepare save user statement", err)
		return errors.NewInternalServerError("database error")
	}
	defer stmt.Close()

	var userId int64 = 0

	err = stmt.QueryRow(user.FirstName, user.LastName, user.Email, user.DateCreated, user.Status, user.Password).Scan(&userId)
	if err != nil {
		logger.Error("error when trying to save user", err)
		return errors.NewInternalServerError("database error")
	}

	user.Id = userId

	return nil

}

func (user *User) Update() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(queryUpdateUser)
	if err != nil {
		logger.Error("error when trying to prepare update user statement", err)
		return errors.NewInternalServerError("database error")
	}
	defer stmt.Close()

	res, err := stmt.Exec(user.FirstName, user.LastName, user.Email, user.Id)

	rowsUpdated, err := res.RowsAffected()
	logger.Info(fmt.Sprintf("Filas afectadas: %d", rowsUpdated))

	if err != nil {
		fmt.Sprintln(err)
		logger.Error("error when trying to update user", err)
		return errors.NewInternalServerError("database error")
	}

	return nil
}

func (user *User) Delete() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(queryDeleteUser)
	if err != nil {
		logger.Error("error when trying to prepare delete user statement", err)
		return errors.NewInternalServerError("database error")
	}
	defer stmt.Close()

	res, err := stmt.Exec(user.Id)

	rowsDeleted, err := res.RowsAffected()
	logger.Info(fmt.Sprintf("Filas eliminadas: %d", rowsDeleted))

	if err != nil {
		logger.Error("error when trying to delete user", err)
		return errors.NewInternalServerError("database error")
	}

	return nil
}

func (user *User) FindByStatus(status string) ([]User, *errors.RestErr) {
	stmt, err := users_db.Client.Prepare(queryFindUserByStatus)
	if err != nil {
		logger.Error("error when trying to prepare find by status user statement", err)

		return nil, errors.NewInternalServerError("database error")
	}
	defer stmt.Close()

	rows, err := stmt.Query(status)
	if err != nil {
		logger.Error("error when trying to find by status user", err)
		return nil, errors.NewInternalServerError("database error")
	}
	defer rows.Close()

	results := make([]User, 0)
	for rows.Next() {
		var user User
		if err := rows.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated, &user.Status); err != nil {
			logger.Error("error when scan user row into users struct", err)
			return nil, errors.NewInternalServerError("database error")
		}
		results = append(results, user)
	}

	if len(results) == 0 {
		return nil, errors.NewNotFoundError(fmt.Sprintf("no users matching status %s", status))
	}

	return results, nil
}
