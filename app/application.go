package app

import (
	"book_store_app/bookstore_users-api/logger"
	"github.com/gin-gonic/gin"
)

var (
	router = gin.Default()
)

func StartApplication() {
	mapUrls()
	logger.Info("start the application")
	router.Run(":8080")
}
