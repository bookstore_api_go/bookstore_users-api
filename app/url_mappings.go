package app

import "book_store_app/bookstore_users-api/controllers/users"

import "book_store_app/bookstore_users-api/controllers/ping"

func mapUrls() {

	router.POST("/user", users.Create)
	router.GET("/user/:user_id", users.Get)
	router.PUT("/user/:user_id", users.Update)
	router.PATCH("/user/:user_id", users.Update)
	router.DELETE("/user/:user_id", users.Delete)
	router.GET("/internal/users/search", users.Search)

	router.GET("/ping", ping.Ping)

}
