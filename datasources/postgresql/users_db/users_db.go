package users_db

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"

	//_ "github.com/go-sql-driver/mysql"
	"log"
)

const (
	postgres_users_username = "postgres_users_username"
	postgres_users_password = "postgres_users_password"
	postgres_users_host     = "postgres_users_host"
	postgres_users_port     = "postgres_users_port"
	postgres_users_db_name  = "postgres_users_db_name"
)

var (
	Client *sql.DB
	/*username = os.Getenv(postgres_users_username)
	password = os.Getenv(postgres_users_password)
	host     = os.Getenv(postgres_users_username)
	port     = os.Getenv(postgres_users_port)
	dbname   = os.Getenv(postgres_users_db_name)*/

	username = "postgres"
	password = "12345678"
	host     = "127.0.0.1"
	port     = "5432"
	dbname   = "users_db"
)

func init() {

	datasourceName := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		host,
		port,
		username,
		password,
		dbname)

	/*datasourceName := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8",
	"postgres",
	"12345678",
	"127.0.0.1:5432",
	"users_db")*/

	var err error
	Client, err = sql.Open("postgres", datasourceName)
	if err != nil {
		panic(err)
	}

	if err = Client.Ping(); err != nil {
		panic(err)
	}

	log.Println("database sucessfully configured")

}
