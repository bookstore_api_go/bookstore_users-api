package main

import (
	"book_store_app/bookstore_users-api/app"
)

func main() {
	app.StartApplication()
}
